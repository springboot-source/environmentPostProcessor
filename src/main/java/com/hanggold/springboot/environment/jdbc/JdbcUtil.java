package com.hanggold.springboot.environment.jdbc;

import java.sql.*;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-07-16 16:04
 * @since
 **/
public class JdbcUtil {

    public static Connection getConnection() {
        Connection connection = null;
        try {
//            connection = DriverManager.getConnection("jdbc:mysql://172.17.125.116:3306/bsxmdb?useUnicode=true&characterEncoding=utf8&useSSL=false", "bsxmdb", "GPRKtx8NjjEY");
            connection = DriverManager.getConnection("jdbc:mysql://192.168.31.218:3306/config?useUnicode=true&characterEncoding=utf8&useSSL=false", "root", "lovety");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

}
