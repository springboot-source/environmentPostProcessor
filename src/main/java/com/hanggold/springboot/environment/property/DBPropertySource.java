package com.hanggold.springboot.environment.property;

import org.springframework.core.env.PropertySource;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-07-16 16:14
 * @since
 **/
public class DBPropertySource  extends PropertySource<DBProperty> {

    private DBProperty dbProperty;


    public DBPropertySource(String name,DBProperty dbProperty){
        super(name,dbProperty);
        this.dbProperty = dbProperty;
    }

    @Override
    public Object getProperty(String name) {
        return dbProperty.getValue(name);
    }
}
