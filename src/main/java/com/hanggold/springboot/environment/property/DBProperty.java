package com.hanggold.springboot.environment.property;

import com.hanggold.springboot.environment.jdbc.JdbcUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 定义一个获取db数据
 *
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-07-16 16:12
 * @since
 **/
public class DBProperty {

    public String getValue(String name) {
        String value = null;
        Connection connection = JdbcUtil.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select value from dis_config where name = ?");
            preparedStatement.setString(1,name);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                value =   resultSet.getString(1);
            }
            System.out.println("你查询的分布式key="+name+" ,value="+value);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return value;
    }


}
