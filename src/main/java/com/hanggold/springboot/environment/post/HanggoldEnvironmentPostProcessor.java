package com.hanggold.springboot.environment.post;

import com.hanggold.springboot.environment.property.DBProperty;
import com.hanggold.springboot.environment.property.DBPropertySource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-07-16 16:04
 * @since
 **/
public class HanggoldEnvironmentPostProcessor implements EnvironmentPostProcessor {

    private static final String OWN_DIS_CONFIG_NAME="ownDisConfigSource";


    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {

        DBProperty dbProperty = new DBProperty();

        DBPropertySource dbPropertySource = new DBPropertySource(OWN_DIS_CONFIG_NAME,dbProperty);

        environment.getPropertySources().addLast(dbPropertySource);

    }
}
