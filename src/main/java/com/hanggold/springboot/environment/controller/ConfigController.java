package com.hanggold.springboot.environment.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("config")
public class ConfigController {

    @Value("${user.name}")
    private String username;

    @Value("${user.province}")
    private String userProvince;

    @GetMapping("username")
    public String configUsername(){
        return username;
    }

    @GetMapping("userprovince")
    public String configUserProvince(){
        return userProvince;
    }


}
