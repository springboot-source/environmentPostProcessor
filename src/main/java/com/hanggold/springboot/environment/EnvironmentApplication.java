package com.hanggold.springboot.environment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-07-16 15:37
 * @since
 **/
@SpringBootApplication
public class EnvironmentApplication {

    public static void main(String[] args) {

        ConfigurableApplicationContext configurableApplicationContext =   SpringApplication.run(EnvironmentApplication.class, args);

        ConfigurableEnvironment configurableEnvironment =   configurableApplicationContext.getEnvironment();

        String value = configurableEnvironment.getProperty("user.name");

        System.out.println("user.name="+ value);


        System.out.println("x");
    }

}
