# 自定义environmentPostProcessor

#### 介绍
基于mysql实现mini版的分布式配置文件组件操作。



##  代码实现

* 创建分布式配置表及插入测试值

  ~~~
  CREATE TABLE `dis_config` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '分布式key值',
    `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '分布式值',
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ;
  
  INSERT INTO `dis_config` VALUES (1, 'user.name', 'helloworld');
  INSERT INTO `dis_config` VALUES (2, 'user.province', '江苏');
  ~~~
  
* 简单实现一个数据库连接操作

  ~~~java
  public class JdbcUtil {
  
      public static Connection getConnection() {
          Connection connection = null;
          try {
  //            connection = DriverManager.getConnection("jdbc:mysql://172.17.125.116:3306/bsxmdb?useUnicode=true&characterEncoding=utf8&useSSL=false", "bsxmdb", "GPRKtx8NjjEY");
              connection = DriverManager.getConnection("jdbc:mysql://1902.168.31.218:3306/config?useUnicode=true&characterEncoding=utf8&useSSL=false", "root", "lovety");
          } catch (SQLException e) {
              e.printStackTrace();
          }
          return connection;
      }
  }
  
  ~~~

  

* 定义基础的属性对象DBProperty

  ~~~~java
  public class DBProperty {
  
      //最终的读取数据库配置走的是这里
      public String getValue(String name) {
          String value = null;
          Connection connection = JdbcUtil.getConnection();
          try {
              PreparedStatement preparedStatement = connection.prepareStatement("select value from dis_config where name = ?");
              preparedStatement.setString(1,name);
              ResultSet resultSet = preparedStatement.executeQuery();
              while (resultSet.next()){
                  value =   resultSet.getString(1);
              }
              System.out.println("你查询的分布式key="+name+" ,value="+value);
          } catch (SQLException e) {
              e.printStackTrace();
          }finally {
              if (connection != null){
                  try {
                      connection.close();
                  } catch (SQLException e) {
                      e.printStackTrace();
                  }
              }
          }
          return value;
      }
  }
  
  ~~~~

* 定义DBPropertySource对象

  ~~~java
  
  // DBPropertySource 继承基础的PropertySource对象
  public class DBPropertySource  extends PropertySource<DBProperty> {
  
      private DBProperty dbProperty;
  
  
      public DBPropertySource(String name,DBProperty dbProperty){
          super(name,dbProperty);
          this.dbProperty = dbProperty;
      }
  
      @Override
      public Object getProperty(String name) {
          return dbProperty.getValue(name);
      }
  }
  
  ~~~

* 定义EnvironmentPostProcessor这个也是最重要，这里赋值一个新的PorpertySource给Environment

  ~~~java
  public class HanggoldEnvironmentPostProcessor implements EnvironmentPostProcessor {
  
      private static final String OWN_DIS_CONFIG_NAME="ownDisConfigSource";
  
  
      @Override
      public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
  
          DBProperty dbProperty = new DBProperty();
  
          DBPropertySource dbPropertySource = new DBPropertySource(OWN_DIS_CONFIG_NAME,dbProperty);
  
          environment.getPropertySources().addLast(dbPropertySource);
  
      }
  }
  ~~~

  * 想让 `HanggoldEnvironmentPostProcessor`被springboot启动时运行需要增加springboot的自动装备的原理。在classpath中新建目录 `META-INF` 然后创建`spring.factories`文件，在自定义的spring.factories中配置如下。

    ~~~java
    org.springframework.boot.env.EnvironmentPostProcessor=\
    com.hanggold.springboot.environment.post.HanggoldEnvironmentPostProcessor
    
    ~~~

    

* 测试controller

  ~~~java
  @RestController
  @RequestMapping("config")
  public class ConfigController {
  
      @Value("${user.name}")
      private String username;
  
      @Value("${user.province}")
      private String userProvince;
  
      @GetMapping("username")
      public String configUsername(){
          return username;
      }
  
      @GetMapping("userprovince")
      public String configUserProvince(){
          return username;
      }
  }
  ~~~

  

 















